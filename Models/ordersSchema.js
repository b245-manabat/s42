const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({
			userId : {
				type : String,
				required : true
			},
			products : [
				{
					productId : {
						type : String,
						required : true
					},
					quantity : {
						type : Number,
						default : 1
					}
				}
			],
			totalAmount : {
				type : Number,
				required : true
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		})

module.exports = mongoose.model("Order", ordersSchema);
